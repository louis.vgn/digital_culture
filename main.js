import { Form, Comparison } from './modules/test.js';

const FacebookConst = document.getElementById('Form');
const ComparisonConst = document.getElementById('Comparison');

const Page = document.getElementById("container");

FacebookConst.addEventListener("click", e => {
    Page.innerHTML = Form();
  });

ComparisonConst.addEventListener("click", e => {
    Page.innerHTML = Comparison();
  });

document.querySelectorAll('a.link-ranking').forEach((aLink,_,arr)=>{
  aLink.onclick =()=>{
    arr.forEach(a=>a.classList.toggle('link-ranking-active',aLink===a))
  }})
